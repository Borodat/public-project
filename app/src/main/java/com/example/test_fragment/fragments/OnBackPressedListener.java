package com.example.test_fragment.fragments;

/**
 * Created by Денис on 11.02.2016.
 */
public interface OnBackPressedListener {
    public void onBackPressed();
}
