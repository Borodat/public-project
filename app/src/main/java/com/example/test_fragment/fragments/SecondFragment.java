package com.example.test_fragment.fragments;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.example.test_fragment.R;
import com.example.test_fragment.adapters.RecyclerAppAdapter;
import com.example.test_fragment.models.BaseData;
import com.example.test_fragment.utils.AppDataActions;
import java.util.ArrayList;



/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link //SecondFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link SecondFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SecondFragment extends Fragment implements OnBackPressedListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private static final String TAG = "myLogs";

    RecyclerView mRecyclerView;
    RecyclerAppAdapter mAdapter;
    RecyclerView.LayoutManager mLayoutManager;



    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    /**
     * Variables collection, storage applications and databases
     */
    private ArrayList<BaseData> ListAppsData =new ArrayList<>();


    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment SecondFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static SecondFragment newInstance(String param1, String param2) {
        SecondFragment fragment = new SecondFragment();

        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);

        return fragment;
    }

    public SecondFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
                if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

        Log.d(TAG, mParam1+mParam2);

        AppDataActions appDataActions = new AppDataActions(getActivity());

        appDataActions.allActions();
        ListAppsData = appDataActions.getListAppsData();

      }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_second, container, false);

        //ListView saveListView = (ListView) view.findViewById(R.id.ListView);
        //saveListView.setAdapter(listadapter);

        mRecyclerView = (RecyclerView) view.findViewById(R.id.my_recycler_view);
        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        mRecyclerView.setHasFixedSize(true);
        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(getActivity());
        // set a linear layout manager
        mRecyclerView.setLayoutManager(mLayoutManager);
        // specify an adapter
        mAdapter = new RecyclerAppAdapter(getActivity(),ListAppsData);
        mRecyclerView.setAdapter(mAdapter);

        return view;
    }

    /**
     * Call the first fragment by pressed Back
     */
    @Override
    public void onBackPressed() {

        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager
                .beginTransaction();
        fragmentTransaction.setCustomAnimations(R.anim.fadein,R.anim.fadeout);

        //fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        FirstFragment firstFragment = FirstFragment.newInstance("SecondFragment","close");
        fragmentTransaction.replace(R.id.container, firstFragment);
        fragmentTransaction.commit();

        //Log.d(TAG, "Back");
    }


}


