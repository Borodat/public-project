package com.example.test_fragment.fragments;

import android.content.Context;
import android.support.v4.app.*;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import com.example.test_fragment.R;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link //FirstFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FirstFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FirstFragment extends Fragment implements View.OnClickListener{
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String TAG = "myLogs";
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    View view;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FirstFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static FirstFragment newInstance(String param1, String param2) {
        FirstFragment fragment = new FirstFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);

        return fragment;
    }

    public FirstFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        Log.d(TAG, mParam1+mParam2);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.d(TAG, "Fragment1 onCreateView() called with: ");
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_first, container, false);

        Button nextButton = (Button) view.findViewById(R.id.buttonView);
        nextButton.setOnClickListener(this);

        return view;

    }

    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub
        try{
            FragmentManager fragmentManager = getFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager
                .beginTransaction();
            //fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
            fragmentTransaction.setCustomAnimations(R.anim.fadein,R.anim.fadeout);
            // добавляем фрагмент
            SecondFragment secondFragment = SecondFragment.newInstance("FirstFragment","close");
            fragmentTransaction.replace(R.id.container, secondFragment);

            fragmentTransaction.commit();
        }catch (Exception ex){
            Log.d(TAG, getString(R.string.load_fragment_error));
        }

    }




    @Override
    public void onDetach() {
        Log.d(TAG, "Fragment1 onDetach() called with: " + "");
        super.onDetach();
    }

    @Override
    public void onDestroyView() {

        /*final Animation animationFadeOut = AnimationUtils.loadAnimation(getActivity(), R.anim.fadeout);
        LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
        LinearLayout linearLayout = (LinearLayout)layoutInflater.inflate(R.layout.fragment_first,null,false);
        linearLayout.startAnimation(animationFadeOut);
        */

        Log.d(TAG, "Fragment1 onDestroyView() called with: " + "");
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        Log.d(TAG, "Fragment1 onDestroy() called with: " + "");
        super.onDestroy();
    }

    @Override
    public void onAttach(Context context) {
        Log.d(TAG, "Fragment1 onAttach() called with: ");
        super.onAttach(context);
    }

    @Override
    public void onPause() {
        Log.d(TAG, "Fragment1 onPause() called with: " + "");
        super.onPause();
    }

    @Override
    public void onResume() {


        Log.d(TAG, "Fragment1 onResume() called with: " + "");
        super.onResume();
    }

    @Override
    public void onStart() {


        Log.d(TAG, "Fragment1 onStart() called with: " + "");
        super.onStart();
    }

    @Override
    public void onStop() {

        Log.d(TAG, "Fragment1 onStop() called with: " + "");
        super.onStop();
    }
}
