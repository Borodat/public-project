package com.example.test_fragment.utils;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.util.Log;

import com.example.test_fragment.R;
import com.example.test_fragment.adapters.AppAdapter;
import com.example.test_fragment.models.BaseData;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by Денис on 14.02.2016.
 */
public class AppDataActions {

    /**
     * Variables flags for print and check applications
     */
    final byte NORM_APP = 0;
    final byte NEW_APP = 1;
    final byte DEL_APP = 2;

    private static final String TAG = "myLogs";

    private Context context;

    private PackageManager packageManager = null;
    /**
     * Variables collection, storage applications and databases
     */
    private ArrayList<BaseData> ListAppsData =new ArrayList<>();
    private ArrayList<BaseData> ListBDData = new ArrayList<>();


    public AppDataActions(Context con){
        this.context = con;

    }

    public void allActions(){
        DBActions dbActions = new DBActions(context);
        ListBDData = dbActions.ReadFromDB();
        LoadApplications(context.getPackageManager());
        checkData();
        Collections.sort(ListAppsData, new SortByAppName());
        dbActions.addNewAppsToDB(ListAppsData);


    }

    public ArrayList<BaseData> getListAppsData(){
        return ListAppsData;
    }

    /**
     * Get list installed applications
     */
    private void LoadApplications (PackageManager pManager){

        packageManager=pManager;
        ProgressDialog progress = ProgressDialog.show(context, null, context.getString(R.string.loading_apps_info));
        try {
            checkForLaunchIntent(packageManager.getInstalledApplications(PackageManager.GET_UNINSTALLED_PACKAGES));
            //listadapter = new AppAdapter(context, R.layout.list_item, ListAppsData);
        } catch (Exception ex) {
            Log.d(TAG, context.getString(R.string.applications_error));
        } finally {
            progress.dismiss();

        }

    }


    /**
     * Get launch intent for package and create a data model
     * @param list A list of all installed applications
     */
    private void checkForLaunchIntent(List<ApplicationInfo> list) {

        for (ApplicationInfo info : list) {
            try {
                if (packageManager.getLaunchIntentForPackage(info.packageName) != null) {

                    BaseData baseData = new BaseData(info.loadLabel(packageManager).toString(),
                            info.packageName, info.loadIcon(packageManager),
                            getAppLastUpdateTime(context,info.packageName));
                    ListAppsData.add(baseData);

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Data checking, find new and remote applications
     */
    private void checkData(){

        try {
            for (BaseData dbData : ListBDData) {

                if (checkDelValue(dbData.getPackageName())) {
                    dbData.setFlags(DEL_APP);
                    ListAppsData.add(dbData);
                    //Log.d(TAG, getString(R.string.del_app) + dbData.getPackageName());
                }
            }

            for (BaseData appData : ListAppsData) {

                if (checkNewValue(appData.getPackageName())) {
                    appData.setFlags(NEW_APP);
                    //Log.d(TAG, getString(R.string.new_app) + appData.getPackageName());
                }
            }
        }catch (Exception ex) {
            Log.d(TAG, ex.getClass()+ex.getMessage());
        }

    }


    public void print(byte state)
    {
        switch (state) {
            case NORM_APP:
                for (BaseData baseData : ListAppsData)
                    if(baseData.getFlags()==0)
                        Log.d(TAG,baseData.getAppName() + " : " + baseData.getPackageName());
                Log.d(TAG, context.getString(R.string.apps_end));
                break;
            case NEW_APP:
                for (BaseData baseData : ListAppsData)
                    if(baseData.getFlags()==1)
                        Log.d(TAG, baseData.getAppName() + " : " + baseData.getPackageName());
                Log.d(TAG, context.getString(R.string.apps_end));

                break;
            case DEL_APP:
                for (BaseData baseData : ListAppsData)
                    if(baseData.getFlags()==2)
                        Log.d(TAG, baseData.getAppName() + " : " + baseData.getPackageName());
                Log.d(TAG, context.getString(R.string.apps_end));
                break;

        }
    }

    /**
     *Package name  of DB cyclically compared with a list of installed applications.
     * At concurrence returns true, false otherwise
     * @param packageName packageName of DB
     * @return return true if matched, false otherwise
     */
    private boolean checkDelValue(String packageName ){

        BaseData app;

        for(int pos=0;pos<ListAppsData.size();pos++){
            app = ListAppsData.get(pos);
            if (app.getPackageName().compareTo(packageName)==0) {
                return false;
            }
        }
        return true;
    }

    /**
     *Package name from the list of installed applications cyclically compared with all values of DB.
     * @param packageName packageName of list of installed applications
     * @return return true if matched, false otherwise
     */
    private boolean checkNewValue(String packageName ){
        int pos;
        BaseData app;

        for(pos=0;pos<ListBDData.size();pos++){
            app = ListBDData.get(pos);
            if (app.getPackageName().compareTo(packageName)==0) {
                return false;
            }
        }
        return true;
    }

    /**
     * Returns lastUpdateTime of installed application
     * @param context Context
     * @param name Name of installed application
     * @return lastUpdateTime of installed application
     */
    private static long getAppLastUpdateTime(Context context,String name) {
        PackageInfo packageInfo;
        try {
            if (Build.VERSION.SDK_INT > 8/*Build.VERSION_CODES.FROYO*/) {
                packageInfo = context.getPackageManager().getPackageInfo(name, 0);
                return packageInfo.lastUpdateTime;
            } else {
                ApplicationInfo appInfo = context.getPackageManager().getApplicationInfo(name, 0);
                String sAppFile = appInfo.sourceDir;
                return new File(sAppFile).lastModified();
            }
        } catch (PackageManager.NameNotFoundException e) {
            return 0;
        }
    }

    /**
     * Comparator of Collection.sort
     */
    class SortByAppName implements Comparator<BaseData> {

        public int compare(BaseData obj1, BaseData obj2) {

            String str1 = obj1.getAppName().toLowerCase();
            String str2 = obj2.getAppName().toLowerCase();

            return str1.compareTo(str2);
        }
    }
}
