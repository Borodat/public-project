package com.example.test_fragment.utils;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.example.test_fragment.R;
import com.example.test_fragment.models.BaseData;

import java.util.ArrayList;

/**
 * Created by Денис on 14.02.2016.
 */
public class DBActions {

    private static final String APP_NAME = "appName";
    private static final String PACKAGE_NAME = "packageName";
    private static final String DATE = "date";
    private static final String DB_NAME = "AppsList";
    private static final String TAG = "myLogs";
    final static byte NEW_APP = 1;



    DBHelper dbHelper;
    private Context context;

    public DBActions(Context con){
        dbHelper=new DBHelper(con);
        this.context = con;
    }
    public  ArrayList<BaseData> ReadFromDB() {

        ArrayList<BaseData> ListDBData = new ArrayList<>();

        try {
            // подключаемся к БД
            SQLiteDatabase db = dbHelper.getWritableDatabase();

            // делаем запрос всех данных из таблицы mytable, получаем Cursor
            Cursor cur = db.query(DB_NAME, null, null, null, null, null, null);
            if (cur.moveToFirst()) {

                // определяем номера столбцов по имени в выборке
                int appNameColIndex= cur.getColumnIndex(APP_NAME);
                int packageNameColIndex = cur.getColumnIndex(PACKAGE_NAME);
                int dateColIndex = cur.getColumnIndex(DATE);

                do {
                    BaseData baseData = new BaseData(cur,appNameColIndex,packageNameColIndex,dateColIndex);
                    ListDBData.add(baseData);
                } while (cur.moveToNext());

                cur.close();
            }

        } catch (Exception ex) {
            Log.d(TAG, ex.getClass()+ex.getMessage());
        }finally {
            dbHelper.close();
            Log.d(TAG, context.getString(R.string.db_close));
            return ListDBData;
        }
    }


    public  void addNewAppsToDB(ArrayList<BaseData> ListAppsData){

        ContentValues cv = new ContentValues();

        try {

            SQLiteDatabase db = dbHelper.getWritableDatabase();
            for (BaseData baseData : ListAppsData) {

                if (baseData.getFlags() == NEW_APP) {
                    cv.put(APP_NAME, baseData.getAppName());
                    cv.put(PACKAGE_NAME, baseData.getPackageName());
                    cv.put(DATE, baseData.getDate());
                    db.insert(DB_NAME, null, cv);
                }
            }
        }catch (Exception ex){
            Log.d(TAG, ex.getClass()+ex.getMessage());
        }finally {
            dbHelper.close();
            Log.d(TAG, context.getString(R.string.db_close));
        }
    }

    public  void clearDB(){
        try {
            // удаляем все записи
            SQLiteDatabase db = dbHelper.getWritableDatabase();
            int clearCount = db.delete(DB_NAME, null, null);
        }catch (Exception ex){
            Log.d(TAG, ex.getClass()+ex.getMessage());
        }finally {
            dbHelper.close();
            Log.d(TAG, context.getString(R.string.db_clear));
        }
    }

}
