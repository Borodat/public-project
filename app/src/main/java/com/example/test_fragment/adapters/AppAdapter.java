package com.example.test_fragment.adapters;

/**
 * Created by Денис on 18.01.2016.
 */
import java.util.ArrayList;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.test_fragment.R;
import com.example.test_fragment.models.BaseData;


public class AppAdapter extends ArrayAdapter{

    private ArrayList appList = null;
    private Context context;


    public AppAdapter(Context context, int resource,ArrayList objects) {
        super(context, resource, objects);

        this.context = context;
        this.appList = objects;

    }


    @Override
    public int getCount() {
        return ((null != appList) ? appList.size() : 0);
    }

    @Override
    public BaseData getItem(int position) {
        return ((null != appList) ? (BaseData)appList.get(position) : null);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;

        if(null == view) {
            LayoutInflater layoutInflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = layoutInflater.inflate(R.layout.list_item, null);
        }

        BaseData data = (BaseData)appList.get(position);

        if(null != data) {

            TextView appName = (TextView) view.findViewById(R.id.app_name);
            TextView packageName = (TextView) view.findViewById(R.id.app_package);
            ImageView iconView = (ImageView) view.findViewById(R.id.app_icon);

            switch (data.getFlags()){
            case 0:
                appName.setText(data.getAppName());
                packageName.setText(data.getPackageName());
                iconView.setImageDrawable(data.getIcon());
                break;
            case 1:
                appName.setText(context.getString(R.string.new_app) + data.getAppName());
                packageName.setText(data.getPackageName());
                iconView.setImageDrawable(data.getIcon());
                break;
            case 2:
                appName.setText(context.getString(R.string.del_app) + data.getAppName());
                packageName.setText(data.getPackageName());
                iconView.setImageDrawable(data.getIcon());
                break;
            default:
                appName.setText(context.getString(R.string.unknown));
                packageName.setText(context.getString(R.string.unknown));
                iconView.setImageDrawable(data.getIcon());

                break;
            }
        }
        return view;
    }
}