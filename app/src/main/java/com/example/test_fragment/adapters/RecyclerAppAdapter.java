package com.example.test_fragment.adapters;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.test_fragment.R;
import com.example.test_fragment.models.BaseData;

import java.util.ArrayList;

/**
 * Created by Денис on 14.02.2016.
 */
public class RecyclerAppAdapter extends RecyclerView.Adapter<RecyclerAppAdapter.ViewHolder> {

    final byte NORM_APP = 0;
    final byte NEW_APP = 1;
    final byte DEL_APP = 2;

    Context context;

    public static class ViewHolder extends RecyclerView.ViewHolder {
        CardView cardView;
        TextView appName;
        TextView packageName;
        ImageView iconView;

        ViewHolder(View itemView) {
            super(itemView);
            cardView = (CardView)itemView.findViewById(R.id.card_view);
            appName = (TextView) itemView.findViewById(R.id.app_name);
            packageName = (TextView) itemView.findViewById(R.id.app_package);
            iconView = (ImageView) itemView.findViewById(R.id.app_icon);
        }
    }

    ArrayList<BaseData> baseDatas;

    public RecyclerAppAdapter(Context con,ArrayList<BaseData> baseData){
        this.baseDatas = baseData;
        this.context = con;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {
        switch (baseDatas.get(i).getFlags()) {
            case NORM_APP:
                viewHolder.appName.setText(baseDatas.get(i).getAppName());
                viewHolder.packageName.setText(baseDatas.get(i).getPackageName());
                viewHolder.iconView.setImageDrawable(baseDatas.get(i).getIcon());
                break;
            case NEW_APP:
                viewHolder.appName.setText(context.getString(R.string.new_app) + baseDatas.get(i).getAppName());
                viewHolder.packageName.setText(baseDatas.get(i).getPackageName());
                viewHolder.iconView.setImageDrawable(baseDatas.get(i).getIcon());
                break;
            case DEL_APP:
                viewHolder.appName.setText(context.getString(R.string.del_app)+baseDatas.get(i).getAppName());
                viewHolder.packageName.setText(baseDatas.get(i).getPackageName());
                viewHolder.iconView.setImageDrawable(baseDatas.get(i).getIcon());
                break;
        }
    }

    @Override
    public int getItemCount() {
        return baseDatas.size();
    }

    @Override
    public RecyclerAppAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View listItem = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_item, viewGroup, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }


}
