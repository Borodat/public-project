package com.example.test_fragment.models;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Денис on 04.02.2016.
 */
 /**
 * Created by Денис on 04.02.2016.
 */
 /**
 * Created by Денис on 04.02.2016.
 */
 /**
 * Created by Денис on 04.02.2016.
 */
public class BaseData extends Activity{
    private String  appName;
    private String packageName;
    byte flags;
    Drawable icon;
    Long date;


    public BaseData(Cursor cur, int appNameColIndex, int packageNameColIndex, int dateColIndex){
        appName = cur.getString(appNameColIndex);
        packageName = cur.getString(packageNameColIndex);
        icon = null;
        date = cur.getLong(dateColIndex);
        flags = 0;
    }

    public BaseData(String aName, String pName, Drawable ic, Long dat){

        appName = aName;
        packageName = pName;
        icon = ic;
        date = dat;
        flags=0;
    }


    public String getPackageName(){
        return packageName;
    }

    public String getAppName(){
        return appName;
    }

    public Drawable getIcon() {
        return icon;
    }

    public void setFlags(byte flags) {
        this.flags = flags;
    }

    public byte getFlags() {
        return flags;
    }

    public Long getDate() {
        return date;
    }
}
